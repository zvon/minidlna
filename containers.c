/* MiniDLNA media server
 * Copyright (C) 2014  NETGEAR
 *
 * This file is part of MiniDLNA.
 *
 * MiniDLNA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * MiniDLNA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiniDLNA. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include "clients.h"
#include "minidlnatypes.h"
#include "scanner.h"
#include "upnpglobalvars.h"
#include "containers.h"
#include "log.h"

#define NINETY_DAYS "7776000"

const char *music_id = MUSIC_ID;
const char *music_all_id = MUSIC_ALL_ID;
const char *music_genre_id = MUSIC_GENRE_ID;
const char *music_artist_id = MUSIC_ARTIST_ID;
const char *music_album_id = MUSIC_ALBUM_ID;
const char *music_plist_id = MUSIC_PLIST_ID;
const char *music_dir_id = MUSIC_DIR_ID;
const char *video_id = VIDEO_ID;
const char *video_all_id = VIDEO_ALL_ID;
const char *video_dir_id = VIDEO_DIR_ID;
const char *image_id = IMAGE_ID;
const char *image_all_id = IMAGE_ALL_ID;
const char *image_date_id = IMAGE_DATE_ID;
const char *image_camera_id = IMAGE_CAMERA_ID;
const char *image_dir_id = IMAGE_DIR_ID;

struct magic_container_s *magic_containers = NULL;
const size_t base_containers = 21;

void basic_magic() {
	magic_containers = malloc( base_containers * sizeof(struct magic_container_s) );
	/* Alternate root container */
	magic_containers[0].name = NULL;
	magic_containers[0].objectid_match = "0";
	magic_containers[0].objectid = &runtime_vars.root_container;
	magic_containers[0].objectid_sql = NULL;
	magic_containers[0].parentid_sql = "0";
	magic_containers[0].refid_sql = NULL;
	magic_containers[0].child_count = NULL;
	magic_containers[0].where = NULL;
	magic_containers[0].orderby = NULL;
	magic_containers[0].max_count = -1;
	magic_containers[0].required_flags = 0;

	/* Recent 50 audio items */
	magic_containers[1].name = "Recently Added";
	magic_containers[1].objectid_match = "1$FF0";
	magic_containers[1].objectid = NULL;
	magic_containers[1].objectid_sql = "\"1$FF0$\" || OBJECT_ID";
	magic_containers[1].parentid_sql = "\"1$FF0\"";
	magic_containers[1].refid_sql = "o.OBJECT_ID";
	magic_containers[1].child_count = "(select null from DETAILS where MIME glob 'a*' and timestamp > (strftime('%s','now') - "NINETY_DAYS") limit 50)";
	magic_containers[1].where = "MIME glob 'a*' and REF_ID is NULL and timestamp > (strftime('%s','now') - "NINETY_DAYS")";
	magic_containers[1].orderby = "order by TIMESTAMP DESC";
	magic_containers[1].max_count = 50;
	magic_containers[1].required_flags = 0;

	/* Recent 50 video items */
	magic_containers[2].name = "Recently Added";
	magic_containers[2].objectid_match = "2$FF0";
	magic_containers[2].objectid = NULL;
	magic_containers[2].objectid_sql = "\"2$FF0$\" || OBJECT_ID";
	magic_containers[2].parentid_sql = "\"2$FF0\"";
	magic_containers[2].refid_sql = "o.OBJECT_ID";
	magic_containers[2].child_count = "(select null from DETAILS where MIME glob 'v*' and timestamp > (strftime('%s','now') - "NINETY_DAYS") limit 50)";
	magic_containers[2].where = "MIME glob 'v*' and REF_ID is NULL and timestamp > (strftime('%s','now') - "NINETY_DAYS")";
	magic_containers[2].orderby = "order by TIMESTAMP DESC";
	magic_containers[2].max_count = 50;
	magic_containers[2].required_flags = 0;

	/* Recent 50 image items */
	magic_containers[3].name = "Recently Added";
	magic_containers[3].objectid_match = "3$FF0";
	magic_containers[3].objectid = NULL;
	magic_containers[3].objectid_sql = "\"3$FF0$\" || OBJECT_ID";
	magic_containers[3].parentid_sql = "\"3$FF0\"";
	magic_containers[3].refid_sql = "o.OBJECT_ID";
	magic_containers[3].child_count = "(select null from DETAILS where MIME glob 'i*' and timestamp > (strftime('%s','now') - "NINETY_DAYS") limit 50)";
	magic_containers[3].where = "MIME glob 'i*' and REF_ID is NULL and timestamp > (strftime('%s','now') - "NINETY_DAYS")";
	magic_containers[3].orderby = "order by TIMESTAMP DESC";
	magic_containers[3].max_count = 50;
	magic_containers[3].required_flags = 0;

	/* Microsoft PlaysForSure containers */
	magic_containers[4].name = NULL;
	magic_containers[4].objectid_match = "4";
	magic_containers[4].objectid = &music_all_id;
	magic_containers[4].objectid_sql = NULL;
	magic_containers[4].parentid_sql = NULL;
	magic_containers[4].refid_sql = NULL;
	magic_containers[4].child_count = NULL;
	magic_containers[4].where = NULL;
	magic_containers[4].orderby = NULL;
	magic_containers[4].max_count = -1;
	magic_containers[4].required_flags = FLAG_MS_PFS;

	magic_containers[5].name = NULL;
	magic_containers[5].objectid_match = "5";
	magic_containers[5].objectid = &music_genre_id;
	magic_containers[5].objectid_sql = NULL;
	magic_containers[5].parentid_sql = NULL;
	magic_containers[5].refid_sql = NULL;
	magic_containers[5].child_count = NULL;
	magic_containers[5].where = NULL;
	magic_containers[5].orderby = NULL;
	magic_containers[5].max_count = -1;
	magic_containers[5].required_flags = FLAG_MS_PFS;

	magic_containers[6].name = NULL;
	magic_containers[6].objectid_match = "6";
	magic_containers[6].objectid = &music_artist_id;
	magic_containers[6].objectid_sql = NULL;
	magic_containers[6].parentid_sql = NULL;
	magic_containers[6].refid_sql = NULL;
	magic_containers[6].child_count = NULL;
	magic_containers[6].where = NULL;
	magic_containers[6].orderby = NULL;
	magic_containers[6].max_count = -1;
	magic_containers[6].required_flags = FLAG_MS_PFS;

	magic_containers[7].name = NULL;
	magic_containers[7].objectid_match = "7";
	magic_containers[7].objectid = &music_album_id;
	magic_containers[7].objectid_sql = NULL;
	magic_containers[7].parentid_sql = NULL;
	magic_containers[7].refid_sql = NULL;
	magic_containers[7].child_count = NULL;
	magic_containers[7].where = NULL;
	magic_containers[7].orderby = NULL;
	magic_containers[7].max_count = -1;
	magic_containers[7].required_flags = FLAG_MS_PFS;

	magic_containers[8].name = NULL;
	magic_containers[8].objectid_match = "8";
	magic_containers[8].objectid = &video_all_id;
	magic_containers[8].objectid_sql = NULL;
	magic_containers[8].parentid_sql = NULL;
	magic_containers[8].refid_sql = NULL;
	magic_containers[8].child_count = NULL;
	magic_containers[8].where = NULL;
	magic_containers[8].orderby = NULL;
	magic_containers[8].max_count = -1;
	magic_containers[8].required_flags = FLAG_MS_PFS;

	magic_containers[9].name = NULL;
	magic_containers[9].objectid_match = "B";
	magic_containers[9].objectid = &image_all_id;
	magic_containers[9].objectid_sql = NULL;
	magic_containers[9].parentid_sql = NULL;
	magic_containers[9].refid_sql = NULL;
	magic_containers[9].child_count = NULL;
	magic_containers[9].where = NULL;
	magic_containers[9].orderby = NULL;
	magic_containers[9].max_count = -1;
	magic_containers[9].required_flags = FLAG_MS_PFS;

	magic_containers[10].name = NULL;
	magic_containers[10].objectid_match = "C";
	magic_containers[10].objectid = &image_date_id;
	magic_containers[10].objectid_sql = NULL;
	magic_containers[10].parentid_sql = NULL;
	magic_containers[10].refid_sql = NULL;
	magic_containers[10].child_count = NULL;
	magic_containers[10].where = NULL;
	magic_containers[10].orderby = NULL;
	magic_containers[10].max_count = -1;
	magic_containers[10].required_flags = FLAG_MS_PFS;

	magic_containers[11].name = NULL;
	magic_containers[11].objectid_match = "F";
	magic_containers[11].objectid = &music_plist_id;
	magic_containers[11].objectid_sql = NULL;
	magic_containers[11].parentid_sql = NULL;
	magic_containers[11].refid_sql = NULL;
	magic_containers[11].child_count = NULL;
	magic_containers[11].where = NULL;
	magic_containers[11].orderby = NULL;
	magic_containers[11].max_count = -1;
	magic_containers[11].required_flags = FLAG_MS_PFS;

	magic_containers[12].name = NULL;
	magic_containers[12].objectid_match = "14";
	magic_containers[12].objectid = &music_dir_id;
	magic_containers[12].objectid_sql = NULL;
	magic_containers[12].parentid_sql = NULL;
	magic_containers[12].refid_sql = NULL;
	magic_containers[12].child_count = NULL;
	magic_containers[12].where = NULL;
	magic_containers[12].orderby = NULL;
	magic_containers[12].max_count = -1;
	magic_containers[12].required_flags = FLAG_MS_PFS;

	magic_containers[13].name = NULL;
	magic_containers[13].objectid_match = "15";
	magic_containers[13].objectid = &video_dir_id;
	magic_containers[13].objectid_sql = NULL;
	magic_containers[13].parentid_sql = NULL;
	magic_containers[13].refid_sql = NULL;
	magic_containers[13].child_count = NULL;
	magic_containers[13].where = NULL;
	magic_containers[13].orderby = NULL;
	magic_containers[13].max_count = -1;
	magic_containers[13].required_flags = FLAG_MS_PFS;

	magic_containers[14].name = NULL;
	magic_containers[14].objectid_match = "16";
	magic_containers[14].objectid = &image_dir_id;
	magic_containers[14].objectid_sql = NULL;
	magic_containers[14].parentid_sql = NULL;
	magic_containers[14].refid_sql = NULL;
	magic_containers[14].child_count = NULL;
	magic_containers[14].where = NULL;
	magic_containers[14].orderby = NULL;
	magic_containers[14].max_count = -1;
	magic_containers[14].required_flags = FLAG_MS_PFS;

	magic_containers[15].name = NULL;
	magic_containers[15].objectid_match = "D2";
	magic_containers[15].objectid = &image_camera_id;
	magic_containers[15].objectid_sql = NULL;
	magic_containers[15].parentid_sql = NULL;
	magic_containers[15].refid_sql = NULL;
	magic_containers[15].child_count = NULL;
	magic_containers[15].where = NULL;
	magic_containers[15].orderby = NULL;
	magic_containers[15].max_count = -1;
	magic_containers[15].required_flags = FLAG_MS_PFS;

	/* Samsung DCM10 containers for Series E(?) */
	magic_containers[16].name = NULL;
	magic_containers[16].objectid_match = "I";
	magic_containers[16].objectid = &image_id;
	magic_containers[16].objectid_sql = NULL;
	magic_containers[16].parentid_sql = NULL;
	magic_containers[16].refid_sql = NULL;
	magic_containers[16].child_count = NULL;
	magic_containers[16].where = NULL;
	magic_containers[16].orderby = NULL;
	magic_containers[16].max_count = -1;
	magic_containers[16].required_flags = FLAG_SAMSUNG_DCM10;

	magic_containers[17].name = NULL;
	magic_containers[17].objectid_match = "A";
	magic_containers[17].objectid = &music_id;
	magic_containers[17].objectid_sql = NULL;
	magic_containers[17].parentid_sql = NULL;
	magic_containers[17].refid_sql = NULL;
	magic_containers[17].child_count = NULL;
	magic_containers[17].where = NULL;
	magic_containers[17].orderby = NULL;
	magic_containers[17].max_count = -1;
	magic_containers[17].required_flags = FLAG_SAMSUNG_DCM10;

	magic_containers[18].name = NULL;
	magic_containers[18].objectid_match = "V";
	magic_containers[18].objectid = &video_id;
	magic_containers[18].objectid_sql = NULL;
	magic_containers[18].parentid_sql = NULL;
	magic_containers[18].refid_sql = NULL;
	magic_containers[18].child_count = NULL;
	magic_containers[18].where = NULL;
	magic_containers[18].orderby = NULL;
	magic_containers[18].max_count = -1;
	magic_containers[18].required_flags = FLAG_SAMSUNG_DCM10;

	/* Jump straight to Music on audio-only devices */
	magic_containers[19].name = NULL;
	magic_containers[19].objectid_match = "0";
	magic_containers[19].objectid = &music_id;
	magic_containers[19].objectid_sql = NULL;
	magic_containers[19].parentid_sql = "0";
	magic_containers[19].refid_sql = NULL;
	magic_containers[19].child_count = NULL;
	magic_containers[19].where = NULL;
	magic_containers[19].orderby = NULL;
	magic_containers[19].max_count = -1;
	magic_containers[19].required_flags = FLAG_AUDIO_ONLY;

	magic_containers[20].name = NULL;
	magic_containers[20].objectid_match = NULL;
	magic_containers[20].objectid = NULL;
	magic_containers[20].objectid_sql = NULL;
	magic_containers[20].parentid_sql = NULL;
	magic_containers[20].refid_sql = NULL;
	magic_containers[20].child_count = NULL;
	magic_containers[20].where = NULL;
	magic_containers[20].orderby = NULL;
	magic_containers[20].max_count = 0;
	magic_containers[20].required_flags = 0;
}

void releaseMagic() {
	for( size_t i = base_containers - 1; magic_containers[i].objectid_match ; i++ ) {
		free(magic_containers[i].name);
		free(magic_containers[i].objectid_match);
		free(magic_containers[i].objectid_sql);
		free(magic_containers[i].parentid_sql);
		free(magic_containers[i].child_count);
		munmap(magic_containers[i].where, 150);
	}
	free(magic_containers);
}

void updateRandom( const char *id, int pos ) {
	struct magic_container_s *cur_mag = &magic_containers[base_containers+pos];
	sprintf( cur_mag->where,
			"d.ID IN ( select DETAIL_ID from OBJECTS where PARENT_ID == '%s'"
			" ORDER BY RANDOM() LIMIT 20 ) and o.OBJECT_ID like "
			"'%s%%'", id, id);
}

void updateRandomName( const char *id, const char *name ) {
	int random_index = -1;
	// +7 because skip "Random "
	while( magic_containers[base_containers + random_index].objectid_match && strcmp(magic_containers[base_containers + random_index].name + 7, name) ) {
		random_index++;
	}
	updateRandom(id, random_index);
}

void addRandom( const char *path, const char *id ) {
	size_t i = 0;
	while( magic_containers[i].objectid_match )
		i++;

	const char *name = path;
	const char *name_temp = name;
	while( *name_temp != '\0' ) {
		if( *name_temp == '/' )
			name = name_temp + 1;
		++name_temp;
	}

	magic_containers[i].name = malloc( 8 + strlen(name) );
	sprintf(magic_containers[i].name, "Random %s", name);

	magic_containers[i].objectid_match = malloc( strlen(id) + 1 );
	strcpy(magic_containers[i].objectid_match, id);

	magic_containers[i].objectid = NULL;

	magic_containers[i].objectid_sql = malloc ( 17 + strlen(id) );
	sprintf(magic_containers[i].objectid_sql, "\"%s$\" || OBJECT_ID", id);

	magic_containers[i].parentid_sql = malloc( 3 + strlen(id) );
	sprintf(magic_containers[i].parentid_sql, "\"%s\"", id);

	magic_containers[i].refid_sql = "o.OBJECT_ID";

	magic_containers[i].child_count = malloc(38);
	sprintf(magic_containers[i].child_count, "( select null from DETAILS LIMIT 20 )");

	magic_containers[i].where = mmap( NULL, 150, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_ANONYMOUS|MAP_SHARED, -1, 0);
	sprintf(magic_containers[i].where, "FALSE");

	magic_containers[i].orderby = "order by TITLE";
	magic_containers[i].max_count = 20;
	magic_containers[i].required_flags = 0;

	void *reall = realloc(magic_containers, (i+2) * sizeof(struct magic_container_s) );
	if( reall == NULL ) {
		fprintf(stderr, "memory allocation error: %s\n",
			magic_containers[i].name);
		i--;
	} else {
		magic_containers = reall;
	}
	i++;

	magic_containers[i].name = NULL;
	magic_containers[i].objectid_match = NULL;
	magic_containers[i].objectid = NULL;
	magic_containers[i].objectid_sql = NULL;
	magic_containers[i].parentid_sql = NULL;
	magic_containers[i].refid_sql = NULL;
	magic_containers[i].child_count = NULL;
	magic_containers[i].where = NULL;
	magic_containers[i].orderby = NULL;
	magic_containers[i].max_count = 0;
	magic_containers[i].required_flags = 0;
}

struct magic_container_s *
in_magic_container(const char *id, int flags, const char **real_id)
{
	size_t len;
	int i;

	for (i = 0; magic_containers[i].objectid_match; i++)
	{
		if (magic_containers[i].required_flags && !(flags & magic_containers[i].required_flags))
			continue;
		if (magic_containers[i].objectid && !(*magic_containers[i].objectid))
			continue;
		DPRINTF(E_MAXDEBUG, L_HTTP, "Checking magic container %d [%s]\n", i, magic_containers[i].objectid_match);
		len = strlen(magic_containers[i].objectid_match);
		if (strncmp(id, magic_containers[i].objectid_match, len) == 0)
		{
			if (*(id+len) == '$')
				*real_id = id+len+1;
			else if (*(id+len) == '\0')
				*real_id = id;
			else
				continue;
			DPRINTF(E_DEBUG, L_HTTP, "Found magic container %d [%s]\n", i, magic_containers[i].objectid_match);
			return &magic_containers[i];
		}
	}

	return NULL;
}

struct magic_container_s *
check_magic_container(const char *id, int flags)
{
	int i;

	for (i = 0; magic_containers[i].objectid_match; i++)
	{
		if (magic_containers[i].required_flags && !(flags & magic_containers[i].required_flags))
			continue;
		if (magic_containers[i].objectid && !(*magic_containers[i].objectid))
			continue;
		DPRINTF(E_MAXDEBUG, L_HTTP, "Checking magic container %d [%s]\n", i, magic_containers[i].objectid_match);
		if (strcmp(id, magic_containers[i].objectid_match) == 0)
		{
			DPRINTF(E_DEBUG, L_HTTP, "Found magic container %d [%s]\n", i, magic_containers[i].objectid_match);
			return &magic_containers[i];
		}
	}

	return NULL;
}
