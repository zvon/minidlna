/* MiniDLNA media server
 * Copyright (C) 2014  NETGEAR
 *
 * This file is part of MiniDLNA.
 *
 * MiniDLNA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * MiniDLNA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiniDLNA. If not, see <http://www.gnu.org/licenses/>.
 */

struct magic_container_s {
	char *name;
	char *objectid_match;
	const char **objectid;
	char *objectid_sql;
	char *parentid_sql;
	const char *refid_sql;
	char *child_count;
	char *where;
	const char *orderby;
	int max_count;
	int required_flags;
};

extern struct magic_container_s *magic_containers;

void basic_magic();
void releaseMagic();
void addRandom( const char *path, const char *id );
void updateRandom( const char *id, int pos );
void updateRandomName( const char *id, const char *name );

struct magic_container_s *in_magic_container(const char *id, int flags, const char **real_id);
struct magic_container_s *check_magic_container(const char *id, int flags);
